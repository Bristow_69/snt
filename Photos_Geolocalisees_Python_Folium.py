#!/usr/bin/env python

import csv

import folium


# CONSTANTES

NOM_FICHIER_CSV  = 'uMap_Photos_Geolocalisees.csv'
NOM_FICHIER_HTML = 'Carte_Photos_Geolocalisees.html'
ZOOM_CARTE       = 15

TUILES      = 'OpenStreetMap'
ATTRIBUTION = '© Contributeurs OpenStreetMap'


# FONCTIONS

def csv_vers_liste(nom_fichier):
    '''Fonction principale qui prend comme argument le nom du fichier
    et le traite pour créer une liste de liste'''
    with open(nom_fichier, 'r') as fichier_csv:
# On ignore l'entête.
        fichier_csv.readline()

        lecture_fichier = csv.reader(fichier_csv, delimiter=',')

        liste_totale = []

        for lignes in lecture_fichier:
# on ignore les éventuelles lignes vides du fichier
            if lignes[3] != '0':
                liste_totale.append(lignes)

    return liste_totale


def coord_centre(liste):
    '''Fonction qui renvoie la moyenne de la latitude et longitude
    de toutes les photos (lignes du fichier), cela permet de centrer
    la carte'''
    somme_lat = 0
    somme_lon = 0

    moyenne_lat = 0
    moyenne_lon = 0

    for lignes in liste:
        latitute  = float(lignes[3])
        longitude = float(lignes[4])

        somme_lat += latitute
        somme_lon += longitude

    nombre_ligne = len(liste)

    moyenne_lat = somme_lat / nombre_ligne
    moyenne_lon = somme_lon / nombre_ligne

    print("Latitude moyenne = {moyenne_lat}, Longitude moyenne = {moyenne_lon}")

    return (moyenne_lat, moyenne_lon)


# CORPS DU CODE
liste_fichier = csv_vers_liste(NOM_FICHIER_CSV)

moyenne_lat , moyenne_lon = coord_centre(liste_fichier)

carte = folium.Map(
    location   = [moyenne_lat, moyenne_lon],
    zoom_start = ZOOM_CARTE,
    tiles      = TUILES,
    attr       = ATTRIBUTION
)

#Boucle de création des marqueurs
for donnees in liste_fichier:
    aide        = 'Clique-moi !'
    titre       = donnees[0]
    description = donnees[1]

# Suppressions des accolades qui étaient nécessaires pour uMap
    image = donnees[2].strip('{}')

    latitute_marqueur  = float(donnees[3])
    longitude_marqueur = float(donnees[4])

    folium.Marker(
        [latitute_marqueur, longitude_marqueur],
        popup="<h4>{}</h4> <br> <i>{}</i> <br><br> <img src='{}' width='300' ".format(titre,description,image),
        tooltip = aide,
        icon    = folium.Icon(color='green', icon='camera')
    ).add_to(carte)

#On sauve la carte au format HTML
carte.save(NOM_FICHIER_HTML)
print("Carte HTML générée dans le dossier courant")
