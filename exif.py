#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  exif.py
#  
# based on https://gist.github.com/erans/983821
# Les photos doivent être mises dans un sous-dossier photos/
# Le Script supprime les images qui n'ont pas de coordonnées GPS (attention donc),
# on peut donc lancer 2 fois le script pour avoir un CSV propre

import exifread
import os


#image_file = 'P90404-080953.jpg'



def _get_if_exist(data, key):
    if key in data:
        return data[key]

    return None


def _convert_to_degress(value):
    """
    Helper function to convert the GPS coordinates stored in the EXIF to degress in float format

    :param value:
    :type value: exifread.utils.Ratio
    :rtype: float
    """
    d = float(value.values[0].num) / float(value.values[0].den)
    m = float(value.values[1].num) / float(value.values[1].den)
    s = float(value.values[2].num) / float(value.values[2].den)

    return d + (m / 60.0) + (s / 3600.0)
    
def get_exif_location(exif_data):
    """
    Returns the latitude and longitude, if available, from the provided exif_data (obtained through get_exif_data above)
    """
    lat = None
    lon = None

    gps_latitude = _get_if_exist(exif_data, 'GPS GPSLatitude')
    gps_latitude_ref = _get_if_exist(exif_data, 'GPS GPSLatitudeRef')
    gps_longitude = _get_if_exist(exif_data, 'GPS GPSLongitude')
    gps_longitude_ref = _get_if_exist(exif_data, 'GPS GPSLongitudeRef')

    if gps_latitude and gps_latitude_ref and gps_longitude and gps_longitude_ref:
        lat = _convert_to_degress(gps_latitude)
        if gps_latitude_ref.values[0] != 'N':
            lat = 0 - lat

        lon = _convert_to_degress(gps_longitude)
        if gps_longitude_ref.values[0] != 'E':
            lon = 0 - lon

    return lat, lon


def get_exif_data(image_file):
    with open(image_file, 'rb') as f:
        exif_tags = exifread.process_file(f)
    return exif_tags 


##Programme principal

fichier_sortie = open('exifs.csv','w')
chemin = os.getcwd() + '/photos/'

print(chemin)

for fichier_photo in os.listdir(chemin):
	photos = str(chemin+fichier_photo)
	print(photos)
	lat, lon = get_exif_location(get_exif_data(photos))
	'''
	#Supprime les photos sans Geoloc, le script doit donc être lancé 2 fois...
	if lat or lon == None:
		os.remove(photos)
	'''
	print("Fichier : {}, Latitude : {}, Longitude : {}". format(fichier_photo, lat, lon))
	fichier_sortie.write(str(fichier_photo)+','+str(lat)+','+ str(lon)+'\n')
