# SNT

Quelques scripts liés à la matière SNT en classe de seconde.

Le script Python utilise le module folium avec en entrée le fichier CSV. Le résultat est une carte HTML : [https://bristow_69.frama.io/snt/Carte_Photos_Geolocalisees.html](https://bristow_69.frama.io/snt/Carte_Photos_Geolocalisees.html)